﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TasksManager.Models;
using Microsoft.AspNet.Identity;

namespace TasksManager.Controllers
{
    [Authorize]
    public class TasksController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Tasks
        public IQueryable<TaskModel> GetTaskModels()
        {
            var username = User.Identity.GetUserName();
            var tasks = db.TaskModels.Where(t => t.ownerName == username);
            return tasks;
        }

        [Route("api/tasks/getid")]
        public int? getId()
        {
            int? id = ++db.TaskModels.OrderByDescending(t => t.id).FirstOrDefault().id;
            if (id == null)
                id = 1;

            return id;
        }//getId()

        // GET: api/Tasks/5
        [ResponseType(typeof(TaskModel))]
        public IHttpActionResult GetTaskModel(int id)
        {
            TaskModel taskModel = db.TaskModels.Find(id);
            var username = User.Identity.GetUserName();
            if (taskModel == null || taskModel.ownerName != username)
            {
                return NotFound();
            }

            return Ok(taskModel);
        }

        // PUT: api/Tasks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTaskModel(int id, TaskModel taskModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != taskModel.id)
            {
                return BadRequest();
            }

            var username = User.Identity.GetUserName();
            if (username != taskModel.ownerName)
            {
                return Unauthorized();
            }

            db.Entry(taskModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaskModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tasks
        [ResponseType(typeof(TaskModel))]
        public IHttpActionResult PostTaskModel(TaskModel taskModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            taskModel.ownerName = User.Identity.GetUserName();
            db.TaskModels.Add(taskModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = taskModel.id }, taskModel);
        }

        // DELETE: api/Tasks/5
        [ResponseType(typeof(TaskModel))]
        public IHttpActionResult DeleteTaskModel(int id)
        {
            TaskModel taskModel = db.TaskModels.Find(id);
            if (taskModel == null)
            {
                return NotFound();
            }

            var username = User.Identity.GetUserName();
            if (username != taskModel.ownerName)
            {
                return Unauthorized();
            }

            db.TaskModels.Remove(taskModel);
            db.SaveChanges();

            return Ok(taskModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TaskModelExists(int id)
        {
            return db.TaskModels.Count(e => e.id == id) > 0;
        }
    }
}