namespace TasksManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskModels", "ownerName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskModels", "ownerName");
        }
    }
}
