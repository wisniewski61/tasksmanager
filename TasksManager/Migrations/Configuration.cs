namespace TasksManager.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TasksManager.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE TaskModels");
            context.TaskModels.AddOrUpdate(t => t.id,
                new TaskModel
                {
                    id = 1,
                    name = "task1",
                    status = "toDo",
                    ownerName = "email@email.com"
                },
                new TaskModel
                {
                    id= 2,
                    name = "task2",
                    status =  "inProgress",
                    ownerName = "email@email.com"
                },
                new TaskModel
                {
                    id = 3,
                    name = "task3",
                    status = "toDo",
                    ownerName = "email@email.com"
                },
                new TaskModel
                {
                    id = 4,
                    name = "task4",
                    status = "done",
                    ownerName = "email@email.com"
                },
                new TaskModel
                {
                    id = 5,
                    name = "task5",
                    status = "inProgress",
                    ownerName = "email@email.com"
                });
        }
    }
}
