﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TasksManager.Models
{
    public class TaskModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string ownerName { get; set; }
    }
}