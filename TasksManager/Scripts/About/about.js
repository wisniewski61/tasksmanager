﻿'use strict';

angular.module('myApp.about', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/about', {
        templateUrl: 'home/about',
        controller: 'aboutCtrl'
    });
}])

.controller('aboutCtrl', [function () {

}]);