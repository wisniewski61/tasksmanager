'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home',
    controller: 'homeCtrl'
  });
}])

.controller('homeCtrl', ['$http', '$window', function ($http, $window) {

}]);