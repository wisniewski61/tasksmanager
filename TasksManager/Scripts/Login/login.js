﻿'use strict';

angular.module('myApp.login', ['ngRoute', 'myApp.auth'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'home/login',
        controller: 'loginCtrl'
    });
}])

.controller('loginCtrl', ['$http', '$window', '$location', 'auth', function ($http, $window, $location, auth) {
    var self = this;
    self.locationService = $location
    self.auth = auth;

    var user = self.user = {
        username: "email@email.com",
        password: "Password1!"
    };

    self.submit = function () {
        var params = { grant_type: "password", userName: user.username, password: user.password };

        $http({
            method: 'POST',
            url: '/token',
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: params,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;' }
        })
        .success(function (data, status, headers, config) {
            $window.sessionStorage.token = data.access_token;
            self.auth.isAuth = true;
            //self.welcome = 'Welcome ' + data.userName + ' Your token is: ' + $window.sessionStorage.token;
            delete self.error;

            // redirect to new view
            self.locationService.path("/tasks");
        })
        .error(function (data, status, headers, config) {
            // Erase the token if the user fails to log in
            delete $window.sessionStorage.token;
            self.auth.isAuth = false;

            // Handle login errors here
            self.error = 'Error: Invalid user or password';
            self.welcome = '';
        });
    };
}]);