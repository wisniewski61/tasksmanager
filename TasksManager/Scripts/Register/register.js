﻿'use strict';

angular.module('myApp.register', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/register', {
        templateUrl: 'home/register',
        controller: 'registerCtrl'
    });
}])

.controller('registerCtrl', ['$http', function ($http) {
    var self = this;

    self.user = {
        username: 'email@email.com',
        password: 'Password1!',
        confirmPassword: 'Password1!'
    };
    var user = self.user;

    self.register = function () {
        $http({
            method: 'POST',
            url: '/api/Account/register',
            data: { Email: user.username, Password: user.password, ConfirmPassword: user.confirmPassword },
            headers: { 'Content-Type': 'application/json' }
        })
        .success(function () {
            delete self.errorMessage;
            self.registerMessage = "Registration succesful";
            self.user = {
                username: '',
                password: '',
                confirmPassword: ''
            };
        })
        .error(function (response) {
            delete self.registerMessage;
            self.errorMessage = response.Message;
        });
    };
}]);