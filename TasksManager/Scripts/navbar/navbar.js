﻿'use strict';

angular.module('myApp.navbar', ['myApp.auth'])

.controller('navbarCtrl', ['$window', 'auth', function ($window, auth) {
    var self = this;
    self.auth = auth;

    self.isTokenPresent = function() {
        if($window.sessionStorage.token)
            self.auth.isAuth = true;
    };

    self.logout = function () {
        delete $window.sessionStorage.token;
        self.auth.isAuth = false;
    };
}]);