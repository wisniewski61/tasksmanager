'use strict';

angular.module('myApp.pricing', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/pricing', {
    templateUrl: 'home/pricing',
    controller: 'pricingCtrl'
  });
}])

.controller('pricingCtrl', [function() {

}]);