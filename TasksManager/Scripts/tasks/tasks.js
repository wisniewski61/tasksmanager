﻿'use strict';

angular.module('myApp.tasks', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/tasks', {
        templateUrl: 'home/tasks',
        controller: 'tasksCtrl'
    });
}])

.controller('tasksCtrl', ['$http', function ($http) {
    var self = this;
    self.tasks;

    self.getTasks = function () {
        self.tasks = [];
        $http.get('/api/tasks').success(function (data, status, header, config) {
            self.tasks = angular.copy(data);
        }).error(function (data, status, header, config) {
            //tasksList = [];
        });
    }//getTasks()

    self.setId = function (task) {
        $http.get('/api/tasks/getId').success(function (data, status, header, config) {
            task.id = data;
        }).error(function (data, status, header, config) {
        });
    }//getTasks()

    self.initHelpers = function () {
        self.addingTaskMode = '';
        self.tasksEditMode = [];
        self.tasksMove = [];
        for (var i = 0; i < self.tasks.length; i++) {
            self.tasksEditMode[self.tasks[i].id] = false;
            self.tasksMove[self.tasks[i].id] = false;
        }//for
    }//initHelpers()

    self.initFunction = function () {
        self.getTasks();
        self.initHelpers();
    };//initFunction()

    self.edit = function (task) {
        for (var i = 0; i < self.tasks.length; i++) {
            if (self.tasksEditMode[self.tasks[i].id] == true) {
                self.cancel(self.tasks[i]);
            }//if
        }//if
        self.temTaskName = task.name;
        self.tasksEditMode[task.id] = true;
    };//edit()

    self.save = function (task) {
        self.tasksEditMode[task.id] = false;
        $http.put('api/tasks/' + task.id, task).success(function (data) {
        });
    };//save()

    self.cancel = function (task) {
        self.tasksEditMode[task.id] = false;
        self.tasksMove[task.id] = false;
        task.name = self.temTaskName;
    };//cancel()

    self.delete = function (task) {
        for (var i = 0; i < self.tasks.length; i++) {
            if (task.id == self.tasks[i].id) {
                $http.delete('api/tasks/' + task.id);
                self.tasks.splice(i, 1);
            }//if
        }//for
    };//delete()

    self.move = function (task) {
        for (var i = 0; i < self.tasks.length; i++) {
            if (self.tasksEditMode[self.tasks[i].id] == true) {
                self.cancel(self.tasks[i]);
            }//if
        }//if
        self.temTaskName = task.name;
        self.tasksMove[task.id] = true;
    };//move()

    self.setStatus = function (task, newStatus) {
        task.status = newStatus;
        self.cancel(task);
        self.save(task);
    }//setStatus()

    self.addingTaskModeOn = function (status) {
        for (var i = 0; i < self.tasks.length; i++) {
            if (self.tasksEditMode[self.tasks[i].id] == true) {
                self.temTaskName = task.name;
                self.cancel(self.tasks[i]);
            }//if
        }//for

        self.newTask = {
            id: 0,
            name: '',
            status: status,
            ownerName: ''
        };

        self.setId(self.newTask);
        self.addingTaskMode = status;
    };//addingTaskMode()

    self.addTask = function () {
        if (!isEmpty(self.newTask.name)) {
            self.tasks.push(angular.copy(self.newTask));
            var task = self.tasks[self.tasks.length - 1];
            $http.post('api/tasks', task).success(function (data) {
                task = data;
            });
            self.cancelAdding();
        }//if
    };//addTask()

    self.cancelAdding = function () {
        delete self.newTask;
        self.addingTaskMode = '';
    };//cancelAdding()

    function isEmpty(str) {
        return (!str || 0 === str.length);
    }//isEmpty()
}]);//tasks